
# Brand Core Service

SpringBoot application exposing end point rest

## Description

This SpringBoot project exposes a GET endpoint to consume an in-memory H2 database.

## Technology used

- Java 17

- Springboot

- h2 database

- Mockito

## How to Run

Compile:

    mvn clean install

## How to Consume

**Service memory database h2 :8082**

    # Localhost
    http://localhost:8082/h2-console

**Manage :8082**

    # Localhost
    http://localhost:8082/<ENDPOINT>