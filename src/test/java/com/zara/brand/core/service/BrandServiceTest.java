package com.zara.brand.core.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.zara.brand.core.dto.BrandDTO;
import com.zara.brand.core.entity.BrandEntity;
import com.zara.brand.core.repository.BrandRepository;
import java.time.LocalDateTime;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class BrandServiceTest {

	@InjectMocks
	private BrandServiceImpl brandServiceImpl;
	@Mock
	private BrandRepository brandRepository;

	@Test
	void should_return_brands_test_1() {

		when(brandRepository.getBrands(any(), any(), any())).thenReturn(createBrandEntityTest1());

		List<BrandDTO> brandDTOList =
			brandServiceImpl.getBrands(1, 35455, LocalDateTime.of(2020, 06, 14, 10, 00, 00));

		assertEquals(45.5F, brandDTOList.get(0).getPrice());
	}
	@Test
	void should_return_brands_test_2() {

		when(this.brandRepository.getBrands(any(),any(),any())).thenReturn(createListBrand());
		when(brandRepository.getBrandsWithPriority(any(), any(), any(), any())).thenReturn(createBrandEntityTest2());

		List<BrandDTO> brandDTOList =
			brandServiceImpl.getBrands(1, 35455, LocalDateTime.of(2020, 06, 14, 16, 00, 00));

		assertEquals(25.45F, brandDTOList.get(0).getPrice());
	}

	@Test
	void should_return_brands_test_3() {


		when(brandRepository.getBrands(any(), any(), any())).thenReturn(createBrandEntityTest3());

		List<BrandDTO> brandDTOList =
			brandServiceImpl.getBrands(1, 35455, LocalDateTime.of(2020, 06, 14, 21, 00, 00));

		assertEquals(35.5F, brandDTOList.get(0).getPrice());
	}

	@Test
	void should_return_brands_test_4() {

		when(this.brandRepository.getBrands(any(),any(),any())).thenReturn(createListBrand());
		when(brandRepository.getBrandsWithPriority(any(), any(), any(), any())).thenReturn(createBrandEntityTest4());

		List<BrandDTO> brandDTOList =
			brandServiceImpl.getBrands(1, 35455, LocalDateTime.of(2020, 06, 15, 10, 00, 00));

		assertEquals(30.5F, brandDTOList.get(0).getPrice());
	}

	@Test
	void should_return_brands_test_5() {

		when(this.brandRepository.getBrands(any(),any(),any())).thenReturn(createListBrand());
		when(brandRepository.getBrandsWithPriority(any(), any(), any(), any())).thenReturn(createBrandEntityTest5());

		List<BrandDTO> brandDTOList =
			brandServiceImpl.getBrands(1, 35455, LocalDateTime.of(2020, 06, 16, 21, 00, 00));

		assertEquals(38.95F, brandDTOList.get(0).getPrice());
	}
	private static List<BrandEntity> createBrandEntityTest1() {

		BrandEntity brandEntity = BrandEntity.builder()
			.id(1L)
			.brandId(1)
			.startDate(LocalDateTime.of(2020,06,14,00,00,00))
			.endDate(LocalDateTime.of(2020,12,31,23,59,59))
			.order(1)
			.productId(1)
			.priority(0)
			.price(45.5F)
			.curr("EUR")
			.build();

		return  List.of(brandEntity);
	}

	private static List<BrandEntity> createBrandEntityTest2() {

		BrandEntity brandEntity = BrandEntity.builder()
			.id(1L)
			.brandId(1)
			.startDate(LocalDateTime.of(2020,06,14,15,00,00))
			.endDate(LocalDateTime.of(2020,06,14,18,30,59))
			.order(1)
			.productId(1)
			.priority(1)
			.price(25.45F)
			.curr("EUR")
			.build();

		return  List.of(brandEntity);
	}

	private static List<BrandEntity> createListBrand() {

		BrandEntity brandEntity_1 = BrandEntity.builder().priority(0).build();
		BrandEntity brandEntity_2 = BrandEntity.builder().priority(1).build();

		return List.of(brandEntity_1,brandEntity_2);
	}
	private static List<BrandEntity> createBrandEntityTest3() {

		BrandEntity brandEntity = BrandEntity.builder()
			.id(1L)
			.brandId(1)
			.startDate(LocalDateTime.of(2020,06,14,00,00,00))
			.endDate(LocalDateTime.of(2020,12,31,23,59,59))
			.order(2)
			.productId(1)
			.priority(0)
			.price(35.5F)
			.curr("EUR")
			.build();

		return  List.of(brandEntity);
	}

	private static List<BrandEntity> createBrandEntityTest4() {

		BrandEntity brandEntity = BrandEntity.builder()
			.id(1L)
			.brandId(1)
			.startDate(LocalDateTime.of(2020,06,15,00,00,00))
			.endDate(LocalDateTime.of(2020,06,15,23,59,59))
			.order(2)
			.productId(1)
			.priority(1)
			.price(30.5F)
			.curr("EUR")
			.build();

		return  List.of(brandEntity);
	}

	private static List<BrandEntity> createBrandEntityTest5() {

		BrandEntity brandEntity = BrandEntity.builder()
			.id(1L)
			.brandId(1)
			.startDate(LocalDateTime.of(2020,06,15,16,00,00))
			.endDate(LocalDateTime.of(2020,12,31,23,59,59))
			.order(4)
			.productId(1)
			.priority(0)
			.price(38.95F)
			.curr("EUR")
			.build();

		return  List.of(brandEntity);
	}
}
