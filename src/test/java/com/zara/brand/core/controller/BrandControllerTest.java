package com.zara.brand.core.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.OK;

import com.zara.brand.core.converter.BrandConverter;
import com.zara.brand.core.dto.BrandDTO;
import com.zara.brand.core.entity.response.BrandResponse;
import com.zara.brand.core.service.BrandService;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

@ExtendWith(MockitoExtension.class)
class BrandControllerTest {

	@InjectMocks
	private BrandController brandController;
	@Mock
	private BrandService brandService;

	@Mock
	private BrandConverter brandConverter;

	@Test
	@Disabled
	void should_get_brands() {

		when(this.brandService.getBrands(any(), any(), any())).thenReturn(createBrandDTO());

		final ResponseEntity<List<BrandResponse>> response =
			this.brandController.getBrands(1, 35455, LocalDateTime.of(2022,06,14,10,00,00,332));


		assertEquals(OK, response.getStatusCode());
	}

	private static List<BrandDTO> createBrandDTO() {

		final BrandDTO brandDTO = BrandDTO.builder()
			.productId(1)
			.brandId(35455)
			.appliedDate(LocalDateTime.of(2022,06,14,10,00,00,332))
			.price(35.5F)
			.build();

		return Collections.singletonList(brandDTO);
	}
}
