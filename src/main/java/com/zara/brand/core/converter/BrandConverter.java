package com.zara.brand.core.converter;

import com.zara.brand.core.dto.BrandDTO;
import com.zara.brand.core.entity.response.BrandResponse;
import com.zara.brand.core.mapper.BrandMapper;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
@RequiredArgsConstructor
public class BrandConverter {

	private BrandMapper brandMapper = Mappers.getMapper(BrandMapper.class);

	public List<BrandResponse> toBrandResponse(List<BrandDTO> brandDTO) {
		return brandMapper.toBrandResponse(brandDTO);
	}
}
