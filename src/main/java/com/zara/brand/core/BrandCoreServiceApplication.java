package com.zara.brand.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BrandCoreServiceApplication {

	public static void main(String[] args) {

		SpringApplication.run(BrandCoreServiceApplication.class, args);
	}

}
