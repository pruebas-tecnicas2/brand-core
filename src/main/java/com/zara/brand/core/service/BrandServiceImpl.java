package com.zara.brand.core.service;

import com.zara.brand.core.dto.BrandDTO;
import com.zara.brand.core.entity.BrandEntity;
import com.zara.brand.core.repository.BrandRepository;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class BrandServiceImpl implements BrandService{

	public static final int ONE = 1;
	private final BrandRepository brandRepository;
	@Override
	public List<BrandDTO> getBrands(Integer brandId, Integer productId, LocalDateTime appliedDate) {

		final List<BrandEntity> brandEntity = this.brandRepository.getBrands(brandId, productId, appliedDate);

		Integer max = brandEntity.stream().map(BrandEntity::getPriority).mapToInt(Integer::intValue).max().orElse(0);

		final List<BrandEntity> brandEntityWithPriority = this.brandRepository.getBrandsWithPriority(brandId, productId, appliedDate, max);

		return Optional.of(brandEntity.size() <= ONE ? brandEntity : brandEntityWithPriority)
			.orElse(Collections.emptyList())
			.stream()
			.map(brandDTO -> BrandDTO.builder()
				.brandId(brandDTO.getBrandId())
				.productId(brandDTO.getProductId())
				.appliedDate(appliedDate)
				.price(brandDTO.getPrice())
				.build()).toList();
	}
}
