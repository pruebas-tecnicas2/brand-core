package com.zara.brand.core.service;

import com.zara.brand.core.dto.BrandDTO;
import java.time.LocalDateTime;
import java.util.List;

public interface BrandService {

	List<BrandDTO> getBrands (final Integer brandId, final Integer productId, final LocalDateTime appliedDate);
}
