package com.zara.brand.core.mapper;

import com.zara.brand.core.dto.BrandDTO;
import com.zara.brand.core.entity.response.BrandResponse;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;

@Generated(
	value = "org.mapstruct.ap.MappingProcessor",
	date = "2023-02-24T12:17:07+0100",
	comments = "version: 1.5.3.Final, compiler: javac, environment: Java 17.0.4.1 (Oracle Corporation)"
)
public class BrandMapperImpl implements BrandMapper{

	@Override
	public List<BrandResponse> toBrandResponse(List<BrandDTO> brandDTOList) {

		if(brandDTOList == null) {
			return null;
		}

		List<BrandResponse> brandResponseList = new ArrayList<BrandResponse>(brandDTOList.size());
		for ( BrandDTO brandDTO : brandDTOList ) {
			brandResponseList.add(brandDTOToBrandResponse(brandDTO));
		}

		return brandResponseList;
	}

	protected BrandResponse brandDTOToBrandResponse(BrandDTO brandDTO) {
		if ( brandDTO == null ) {
			return null;
		}

		BrandResponse.BrandResponseBuilder brandResponse = BrandResponse.builder();

		brandResponse.brandId(brandDTO.getBrandId());
		brandResponse.productId(brandDTO.getProductId());
		brandResponse.appliedDate(brandDTO.getAppliedDate());
		brandResponse.price(brandDTO.getPrice());

		return brandResponse.build();
	}
}
