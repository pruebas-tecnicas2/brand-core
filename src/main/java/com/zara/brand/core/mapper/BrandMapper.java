package com.zara.brand.core.mapper;

import com.zara.brand.core.dto.BrandDTO;
import com.zara.brand.core.entity.response.BrandResponse;
import java.util.List;
import org.mapstruct.Mapper;

@Mapper
public interface BrandMapper {

	List<BrandResponse> toBrandResponse(List<BrandDTO> brandDTO);
}
