package com.zara.brand.core.repository;

import com.zara.brand.core.entity.BrandEntity;
import java.time.LocalDateTime;
import java.util.List;
import java.util.OptionalInt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface BrandRepository extends JpaRepository<BrandEntity, Long> {

	/*@Query("SELECT brand FROM BrandEntity brand "
		+ " WHERE brand.brandId = :brandId"
		+ " AND brand.productId = :productId AND :appliedDate >= brand.startDate AND :appliedDate <= brand.endDate")*/
	@Query("SELECT brand FROM BrandEntity brand "
		+ " WHERE brand.brandId = :brandId"
		+ " AND brand.productId = :productId AND :appliedDate BETWEEN brand.startDate AND brand.endDate")
	List<BrandEntity> getBrands(final Integer brandId, final Integer productId, final LocalDateTime appliedDate);

	@Query("SELECT brand FROM BrandEntity brand "
		+ " WHERE brand.brandId = :brandId"
		+ " AND brand.productId = :productId AND :appliedDate BETWEEN brand.startDate AND brand.endDate "
		+ "AND brand.priority = :priority")
	List<BrandEntity> getBrandsWithPriority(final Integer brandId, final Integer productId, final LocalDateTime appliedDate,
											final Integer priority);
}
