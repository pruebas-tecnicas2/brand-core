package com.zara.brand.core.dto;

import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class BrandDTO {

	private Integer productId;

	private Integer brandId;

	private LocalDateTime appliedDate;

	private Float price;
}
