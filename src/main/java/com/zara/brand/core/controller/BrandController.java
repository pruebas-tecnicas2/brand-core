package com.zara.brand.core.controller;

import static org.springframework.http.HttpStatus.OK;

import com.zara.brand.core.converter.BrandConverter;
import com.zara.brand.core.dto.BrandDTO;
import com.zara.brand.core.entity.response.BrandResponse;
import com.zara.brand.core.service.BrandService;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class BrandController {

	private final BrandService brandService;
	private final BrandConverter brandConverter;


	@GetMapping("/brands")
	@ResponseStatus(OK)
	public ResponseEntity<List<BrandResponse>> getBrands(@RequestParam(value = "brandId") final Integer brandId,
														 @RequestParam(value = "productId") final Integer productId,
														 @RequestParam(value = "appliedDate") @DateTimeFormat(iso = ISO.DATE_TIME) final LocalDateTime appliedDate) {

		final List<BrandDTO> brandDTOS = this.brandService.getBrands(brandId,productId,appliedDate);

		return ResponseEntity.ok(this.brandConverter.toBrandResponse(brandDTOS));
	}
}
